+function() {

  angular.module("bandTogetherApp", []).
    controller("indexController", indexController);

  indexController.$inject = ["$scope", "$http"];

  function indexController($scope, $http) {

    $scope.SPLIT_SIZE = 3;
    $scope.lon = undefined;
    $scope.lat = undefined;
    $scope.selectedProfileIndex = undefined;
    $scope.selectedProfile = undefined;
    $scope.splitProfiles = undefined;
    $scope.category = undefined;
    $scope.profiles = undefined;
    $scope.events = undefined;
    $scope.displayComponent = displayComponent;
    $scope.setSelectedProfileIndex = setSelectedProfileIndex;
    $scope.setCategory = setCategory;
    $scope.getLocation = getLocation;
    $scope.getTestLocation = getTestLocation;
    $scope.getNextProfile = getNextProfile;
    $scope.getPrevProfile = getPrevProfile;
    $scope.$watch("selectedProfileIndex", watchSelectedProfileIndex);
    $scope.$watch("category", watchCategory)
    $scope.$watch("profiles", watchProfiles);
    $scope.$watchGroup(["lon", "lat"], watchLonLat);

    function watchLonLat(newVal, oldVal) {
      if (newVal !== oldVal) {
        getNearby("user", $scope.lon, $scope.lat, userCallback);
      }
    }

    function watchProfiles(newVal, oldVal) {
      if (newVal !== oldVal) {
        setSplitProfiles(splitData(newVal, $scope.SPLIT_SIZE, true, "distance"));
      }
    }

    function watchCategory(newVal, oldVal) {
      if (newVal !== oldVal) {
        if ($scope.lon && $scope.lat) {
          getNearby("event", $scope.lon, $scope.lat, eventCallback);
        }
      }
    }

    function watchSelectedProfileIndex(newVal, oldVal) {
      if (newVal !== oldVal) {
        setSelectedProfile($scope.profiles[newVal]);
      }
    }

    function setLonAndLat(lon, lat) {
      $scope.lon = lon;
      $scope.lat = lat;
    }

    function setProfiles(profiles) {
      $scope.profiles = profiles;
    }

    function setSplitProfiles(splitProfiles) {
      $scope.splitProfiles = splitProfiles;
    }

    function setSelectedProfile(selectedProfile) {
      $scope.selectedProfile = selectedProfile;
    }

    function setSelectedProfileIndex(selectedProfileIndex) {
      $scope.selectedProfileIndex = selectedProfileIndex;
    }

    function setCategory(category) {
      $scope.category = category;
    }

    function getNextProfile() {
      if ($scope.selectedProfileIndex < ($scope.profiles.length-1)) {
        setSelectedProfileIndex($scope.selectedProfileIndex+1);
      }
    }

    function getPrevProfile() {
      if ($scope.selectedProfileIndex > 0) {
        setSelectedProfileIndex($scope.selectedProfileIndex-1);
      }
    }

    function getTestLocation() {
      setLonAndLat(-74.09732968426692, 38.41508222263303);
    }

    function getLocation() {
      if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(pos) {
            setLonAndLat(pos.coords.longitude, pos.coords.latitude);
            $scope.$apply();
          });
      } else {
          console.log("browser (or you) says no...");
      }
    }

    function displayComponent(container) {
      var containers = $(".container");

      for (var i in containers) {
        $("#" + containers[i].id).hide();
      }

      $(container).show();
    }

    function splitData(data, splitSize, sortData, sortProp) {
      sortData = typeof sortData === "undefined" ? false : sortData;

      if (sortData && sortProp) {
        data = data.sort(function(a, b) {
            if (a[sortProp] > b[sortProp]) {
              return 1;
            }
            if (a[sortProp] < b[sortProp]) {
              return -1;
            }
            return 0;
        });
      }

      var splits = [];

      for (var i = 0; i < data.length; i += splitSize) {
        splits.push(data.slice(i, i+splitSize));
      }

      return splits;
    }

    /*
    *
    * USER & EVENT SERVICES
    *
    */

    function eventCallback(response) {
        console.log(response);
        var events = [];
        var hits = response.hits.hits;
        var EVENTS_DIR = "http://localhost:3000/data/event/";
        for (var i in hits) {
          var evnt = hits[i]._source;

          if (evnt.category === $scope.category) {
            evnt.distance = hits[i].sort[0];
            evnt.picture = EVENTS_DIR + evnt.picture;
            events.push(evnt);
          }
        }
        $scope.events = events;
    }

    function userCallback(response) {
        console.log(response);
        var profiles = [];
        var hits = response.hits.hits;
        var PROFILE_PIC_DIR = "http://localhost:3000/data/profile/";
        for (var i in hits) {
          var profile = hits[i]._source;
          profile.distance = hits[i].sort[0];
          profile.profilepic = PROFILE_PIC_DIR + profile.profilepic;
          profiles.push(profile);
        }
        setProfiles(profiles);
    }

    function getNearby(index, lon, lat, callback) {
        var UNIT = "mi";
        var radius = "300" + UNIT;

        $http.post("http://localhost:9200/sp/" + index + "/_search?size=1000",
          {
            "query": {
              "filtered": {
                "filter": {
                  "geo_distance": {
                    "distance": radius, 
                    "location": { 
                      "lat": lat,
                      "lon": lon
                    }
                  }
                }
              }
            },
              "sort" : [
                {
                  "_geo_distance": {
                    "location": {
                      "lat": lat,
                      "lon": lon
                    },
                    "order": "asc",
                    "unit" : UNIT,
                    "distance_type": "plane"
                  }
                }
            ]
          }
        )
        .success(callback);
    }



    /*
    *
    * MAP SERVICES
    *
    */

    function getDistance(lon1, lat1, lon2, lat2, returnMiles) {
      returnMiles = typeof returnMiles === "undefined" ? true : returnMiles;
      var RADIANS_PER_DEGREE = (Math.PI / 180);
      var EARTH_RADIUS_MILES = 3961;
      var EARTH_RADIUS_KM = 6373;
      var EARTH_RADIUS = returnMiles ? EARTH_RADIUS_MILES : EARTH_RADIUS_KM;
      lon1 *= RADIANS_PER_DEGREE;
      lat1 *= RADIANS_PER_DEGREE;
      lon2 *= RADIANS_PER_DEGREE;
      lat2 *= RADIANS_PER_DEGREE;
      var dlon = lon2 - lon1; 
      var dlat = lat2 - lat1;
      var alpha = Math.pow(Math.sin(dlat/2),2) + Math.cos(lat1) * Math.cos(lat2) * Math.pow(Math.sin(dlon/2),2); 
      var angularDistance = 2 * Math.atan2( Math.sqrt(alpha), Math.sqrt(1-alpha)); 
      return EARTH_RADIUS * angularDistance;
    }
  
    $scope.shouldZoom = true;

    $scope.renderMap = function(lon, lat, events) {
      var width = 960;
      var height = 600;

      var projection = d3.geo.albersUsa()
        .scale(1280)
          .translate([width/2, height/2]);

      var path = d3.geo.path()
          .projection(null);

      var drag = d3.behavior.drag()
          .on("dragstart", function (d) {
            d3.event.sourceEvent.stopPropagation();
            d3.select(this).classed("dragging", true);
          })
          .on("drag", function dragged(d) {
            d3.select(this).attr("cx", d.x = d3.event.x).attr("cy", d.y = d3.event.y);
          })
          .on("dragend", function dragended(d) {
            d3.select(this).classed("dragging", false);
          });

      var zoom = d3.behavior.zoom()
          .scaleExtent([1, 20])
          .on("zoom", function() {
            svg.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
          });

      d3.selectAll("svg").remove();
      
      var svg = d3.select("#map").append("svg")
          .attr("width", width)
          .attr("height", height)
          .append("g")
            .call(zoom);

      var scale = 20;
      var values = projection([lon, lat]);
      values[0] = values[0]*-scale + width/2;
      values[1] = values[1]*-scale + height/2;

      if ($scope.shouldZoom) {
        svg.transition().duration(4000).call(zoom.translate(values).scale(scale).event);
        $scope.shouldZoom = false;
      } else {
        svg.transition().duration(0).call(zoom.translate(values).scale(scale).event);
      }

      d3.json("data/us.json", function(error, us) {
        if (error) return console.error(error);

        svg.append("path")
          .datum(topojson.feature(us, us.objects.nation))
          .attr("class", "land")
          .attr("d", path);

        svg.append("path")
          .datum(topojson.mesh(us, us.objects.states, function(a, b) { return a !== b; }))
          .attr("class", "border border--state")
          .attr("d", path);
        
        svg.append("circle")
          .attr("class", "user")
          .attr("transform", "translate(" + projection([lon, lat]) + ")")
          .attr("r", "0.3");

        for (var i in events) {
          svg.append("circle")
            .attr("class", "event")
            .attr("transform", "translate(" + projection([events[i].location.lon, events[i].location.lat]) + ")")
            .attr("r", "0.3");
        }

      });

    }

  }

}();